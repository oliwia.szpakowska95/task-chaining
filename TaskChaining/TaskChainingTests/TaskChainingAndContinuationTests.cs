﻿using NUnit.Framework;
#pragma warning disable CS8625

/// <summary>
/// This class tests logic of TaskChainingAndContinuation.
/// </summary>
namespace TaskChainingTests
{
    [TestFixture]
    public class TaskChainingAndContinuationTest
    {
        TaskChainingLibrary.TaskChainingAndContinuationLibrary tasks;
        Random randomNumber;

        [SetUp]
        public void SetUp()
        {
            tasks = new TaskChainingLibrary.TaskChainingAndContinuationLibrary();
            randomNumber = new Random();
        }

        [Test]
        public void ArrayMultiplication_ForInvalidArgument_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => tasks.MultiplyArray(null, 3));
        }

        [Test]
        public void InitializationWithRandomNumber_ForInvalidArgument__ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => tasks.InitializeWithRandom(null));
        }

        [Test]
        public void ArraySorting_ForInvalidArgument__ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => tasks.SortArrayByAscending(null));
        }

        [Test]
        public void ArrayAverage_ForInvalidArgument_ThrowsNullException()
        {
            Assert.Throws<ArgumentNullException>(() => tasks.CalculateAverage(null));
        }

        [Test]
        public void CreateNewArray_ForInvalidArgument_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => tasks.CreateNewArray(int.MinValue));
            Assert.Throws<ArgumentException>(() => tasks.CreateNewArray(-1));
            Assert.Throws<ArgumentException>(() => tasks.CreateNewArray(0));
        }

        [Test]
        public void TaskChainingRandom_VeryfiesTheDelegate()
        {
            Assert.DoesNotThrowAsync(async () => await Task.Run(() => tasks.CreateNewArray())
                .ContinueWith(array => tasks.InitializeWithRandom(array.Result))
                .ContinueWith(initializatedArray => tasks.MultiplyArray(initializatedArray.Result, randomNumber.Next()))
                .ContinueWith(multipliedArray => tasks.SortArrayByAscending(multipliedArray.Result))
                .ContinueWith(result => tasks.CalculateAverage(result.Result)));
        }

        [Test]
        public void InitializeWithRandom_IsPositiveArrayInitialization()
        {
            var array = new int[10];
            var initializedArray = tasks.InitializeWithRandom(array);
            bool isAnyValueZero = false;

            for (int i = 0; i < initializedArray.Length; i++)
            {
                if (initializedArray[i] != 0)
                {
                    isAnyValueZero = false;
                }
            }

            Assert.That(isAnyValueZero, Is.False);
        }

        [Test]
        public void MultiplyArray_ArrayMultiplicationForPositiveValuesTest()
        {
            int[] array = new int[5];
            array[0] = 1;
            array[1] = 2;
            array[2] = 3;
            array[3] = 4;
            array[4] = 5;
            long sum = 0;

            var multiplier = 2;
            var resultArray = tasks.MultiplyArray(array, multiplier);
            foreach (var result in resultArray)
            {
                sum += result;
            }

            Assert.That(sum, Is.EqualTo(30));
        }

        [Test]
        public void MultiplyArray_ArrayMultiplicationForNegativeValuesTest()
        {
            int[] array = new int[5];
            array[0] = -1;
            array[1] = -2;
            array[2] = -3;
            array[3] = -4;
            array[4] = -5;
            long sum = 0;

            var multiplier = -2;
            var resultArray = tasks.MultiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                sum += result;
            }

            Assert.That(sum, Is.EqualTo(30));
        }

        [Test]
        public void MultiplyArray_ArrayMultiplicationForZeroMultiplicationTest()
        {
            int[] array = new int[5];
            array[0] = -1;
            array[1] = -2;
            array[2] = -3;
            array[3] = -4;
            array[4] = -5;
            long sum = 0;

            var multiplier = 0;
            var resultArray = tasks.MultiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                sum += result;
            }
            Assert.That(sum, Is.EqualTo(0));
        }

        [Test]
        public void SortArrayByAscending_IsArraySorted()
        {
            var array = new long[6];
            array[0] = int.MinValue;
            array[1] = int.MaxValue;
            array[2] = 0;
            array[3] = -50;
            array[4] = -1;
            array[5] = 220;

            var expectedArrayAfterSorting = new long[6] { int.MinValue, -50, -1, 0, 220, int.MaxValue };
            var sortedArrayByTask = tasks.SortArrayByAscending(array);
            Assert.That(sortedArrayByTask, Is.EqualTo(expectedArrayAfterSorting));
        }

        [Test]
        public void CalculateAverage_IsAverageCorrect()
        {
            var array = new long[5];
            array[0] = 10;
            array[1] = 10;
            array[2] = 10;
            array[3] = 10;
            array[4] = 10;

            var average = tasks.CalculateAverage(array);
            Assert.That(average, Is.EqualTo(10));
        }
    }
}
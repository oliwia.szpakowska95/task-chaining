﻿using System;
using System.Threading.Tasks;


TaskChainingLibrary.TaskChainingAndContinuationLibrary task = new();
var randomNumber = new Random();

var averageValue = await Task.Run(() => task.CreateNewArray(10))
               .ContinueWith(array =>
               {
                   Console.WriteLine("New array created.");
                   return task.InitializeWithRandom(array.Result);
               })
               .ContinueWith(initializatedArray =>
               {
                   Console.WriteLine("\nArray initialized by random number:");
                   foreach (var item in initializatedArray.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item}");
                   }
                   Console.WriteLine();
                   var multiplier = randomNumber.Next(1000);
                   Console.WriteLine($"\nRandom multiplier: {multiplier}");
                   return task.MultiplyArray(initializatedArray.Result, multiplier);
               })
               .ContinueWith(multipliedArray =>
               {
                   Console.WriteLine($"\nMultiplied array result:");
                   foreach (var item in multipliedArray.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item}");
                   }
                   Console.WriteLine();
                   return task.SortArrayByAscending(multipliedArray.Result);
               })
               .ContinueWith(result =>
               {
                   Console.WriteLine($"\nArray sorted by ascending:");
                   foreach (var item in result.Result.AsEnumerable())
                   {
                       Console.WriteLine($"{item}");
                   }
                   Console.WriteLine();
                   return task.CalculateAverage(result.Result);
               });

Console.WriteLine($"\nAverage value: {averageValue}");
Console.ReadLine();
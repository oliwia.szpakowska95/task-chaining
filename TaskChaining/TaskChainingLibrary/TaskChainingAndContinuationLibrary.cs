﻿#pragma warning disable CA1822
namespace TaskChainingLibrary
{
    /// <summary>
    /// Represents library class that executes a chain of four tasks:
    /// Task 1: creates an array of 10 random integers.
    /// Task 2: multiplies the array by a randomly generated number.
    /// Task 3: sorts the array by ascending.
    /// Task 4: calculates the average value.
    /// </summary>
    public class TaskChainingAndContinuationLibrary
    {
        /// <summary>
        /// This method generates new array.
        /// </summary>
        /// <param name="size">Array size.</param>
        /// <exception cref="ArgumentException">Throw when size is less than 1.</exception>
        public int[] CreateNewArray(int size = 10)
        {
            if (size < 1)
            {
                throw new ArgumentException("Array size cannot be smaller then 1", nameof(size));
            }
            return new int[size];
        }

        /// <summary>
        /// This method fill an array by random numbers.
        /// </summary>
        /// <param name="size">Array size.</param>
        /// <exception cref="ArgumentNullException">Throw when size is less than 1.</exception>
        public int[] InitializeWithRandom(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            var randomNumber = new Random();
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = randomNumber.Next();
            }
            return array;
        }

        /// <summary>
        /// This method multiplies the array by a randomly generated number.
        /// </summary>
        /// <param name="multiplier">Multiplier number.</param>
        /// <param name="array">Array of integers.</param>
        /// <exception cref="ArgumentNullException">Throw when count or array is null.</exception>
        public long[] MultiplyArray(int[] array, int multiplier)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            var multipliedArray = new long[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                multipliedArray[i] = array[i] * (long)multiplier;
            }
            return multipliedArray;
        }


        /// <summary>
        /// This method sorts the array by ascending.
        /// </summary>
        /// <param name="array">Array of integers.</param>
        /// <exception cref="ArgumentNullException">Throw when count or array is null.</exception>
        public long[] SortArrayByAscending(long[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            var sortedArray = new long[array.Length];
            Array.Copy(array, 0, sortedArray, 0, array.Length);
            Array.Sort(sortedArray, 0, sortedArray.Length);
            
            return sortedArray;
        }

        /// <summary>
        /// This method calculates the average value.
        /// </summary>
        /// <param name="array">Array of integers.</param>
        /// <exception cref="ArgumentNullException">Throw when count or array is null.</exception>
        public decimal CalculateAverage(long[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            var average = Convert.ToDecimal(array[0]);
            for (var i = 1; i < array.Length; i++)
            {
                average = (average + array[i]) / 2;
            }
            return average;
        }
    }
}